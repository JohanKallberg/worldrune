using UnityEngine;
using System.Collections.Generic;

public class HotbarButton : MonoBehaviour
{
    public TileType stack;
    public int count;
}

public class UITileStack
{
    TileType type;
    int count;
}