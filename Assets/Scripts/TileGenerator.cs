﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGenerator : MonoBehaviour
{

    public const int gridSize = 128;
    public const float tileSize = 4f;
    [HideInInspector]
    public const float tilePosOffset = -gridSize * tileSize / 2;
    public Tile[] tiles;
    public TileType[] tilesType;
    Transform tilesContainer;
    Transform newTilesContainer;
    List<Tile> pendingCreation = new List<Tile>();
    List<TileType> activeTileTypes = new List<TileType>(); // use an int and shifted bool as mask instead of int list
    public Dictionary<int,GameObject> runes = new Dictionary<int,GameObject>();

    private float mMoveDuration = 1.7f;
    void Start()
    {
        tilesContainer = new GameObject("tilesContainer").transform;
        newTilesContainer = new GameObject("newTilesContainer").transform;

        activeTileTypes.Add(TileType.stone_floor);//note multiple places
         
        tiles = new Tile[gridSize * gridSize];
        tilesType = new TileType[gridSize*gridSize];
        for (int iTile = 0; iTile < tiles.Length; iTile++)
        {
            tiles[iTile] = new Tile(iTile, TileType.stone_floor, transform);
            tilesType[iTile] = TileType.stone_floor;
        }

        CreateRune(Color.grey, TileType.stone_floor);
        CreateRune(Color.green, TileType.grass_floor);
        CreateRune(Color.blue, TileType.water);
        CreateRune(Color.black, TileType.cliff);
        CreateRune(Color.white, TileType.ice);

        for(int i = 0; i < 100; i++){
            CreateSimpleMob( Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f));
        }
    }

    public static Vector3 CalcPosition(int iTile)
    {
        return new Vector3(iTile % gridSize * tileSize + tilePosOffset, 0, iTile / gridSize * tileSize + tilePosOffset);
    }
    public static int FindTileIndex(Vector3 position)
    {
        int row = (int)Mathf.Round((position.x - tilePosOffset) / tileSize);
        int col = (int)Mathf.Round((position.z - tilePosOffset) / tileSize);
        return row + col * gridSize;
    }

    public void ReplaceTile(int iTile, TileType type)
    {
        if (tilesType[iTile] == type)
        {
            return;
        }
        var newTile = new Tile(iTile, type, tiles[iTile].transform.parent);

        DestroyImmediate(tiles[iTile].obj);
        tiles[iTile] = newTile;
        tilesType[iTile] = type;
    }

    private GameObject CreateRune(Color color, TileType type)
    {
        GameObject result = GameObject.CreatePrimitive(PrimitiveType.Cube);
        result.transform.localScale *= tileSize * 0.5f;
        Material mat = new Material(Shader.Find("Standard"));
        mat.color = color;
        result.GetComponent<Renderer>().material = mat;
        result.name = "rune_"+type.ToString();
        result.tag = "rune";
        int tilesFromCenter = 6;
        int row = gridSize / 2 + Random.Range(-tilesFromCenter, tilesFromCenter);
        int col = gridSize / 2 + Random.Range(-tilesFromCenter, tilesFromCenter);
        int tileIndex = row+col*gridSize;
        while(runes.ContainsKey(tileIndex)){
            tileIndex = row+col*gridSize;
        }
        result.transform.position = CalcPosition(tileIndex) +  Vector3.up * 0.5f;
        var runeComp = result.AddComponent<Rune>();
        runeComp.type = type;
        runes.Add(tileIndex,result);
        return result;
    }

     private GameObject CreateSimpleMob(Color color)
    {
        GameObject result = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        result.transform.localScale *= tileSize * 0.3f;
        Material mat = new Material(Shader.Find("Standard"));
        mat.color = color;
        result.GetComponent<Renderer>().material = mat;
        int tilesFromCenter = 10;
        int row = gridSize / 2 + Random.Range(-tilesFromCenter, tilesFromCenter);
        int col = gridSize / 2 + Random.Range(-tilesFromCenter, tilesFromCenter);
        int tileIndex = row+col*gridSize;
        result.transform.position = CalcPosition(tileIndex) +  Vector3.up * 0.5f;
        var mob = result.AddComponent<SimpleMob>();
        return result;
    }

    public void GenerateWorld(Rune trigger)
    {
        if (newTilesContainer.position != tilesContainer.position || null == trigger) //we are moving new stuff into place
            return;

        trigger.gameObject.SetActive(false);
        activeTileTypes.Add(trigger.type);
        if (TileType.cliff == trigger.type || TileType.ice == trigger.type)
        {
            int mazeSize = Random.Range(4, 20);
            SpawnMaze(GenerateMaze(mazeSize), FindTileIndex(trigger.transform.position) - mazeSize / 2 * (1 + gridSize), trigger.type);
            StartCoroutine(MoveTiles());
            return;
        }

        foreach (TileType iTileType in activeTileTypes)
        {
            for (int iTile = 0; iTile < tiles.Length; iTile++)
            {
                if (tilesType[iTile] == iTileType)
                {
                    continue;
                }
                else if (Random.value < 0.1f) //should Spawn
                {
                    pendingCreation.Add(new Tile(iTile, iTileType, newTilesContainer));
                }
            }
        }
        StartCoroutine(MoveTiles());
    }

    private void SpawnMaze(bool[] maze, int offset, TileType wallType)
    {
        int side = (int)Mathf.Sqrt(maze.Length);
        for (int iTile = 0; iTile < maze.Length; iTile++)
        {
            int gridIndex = offset + (iTile % side) + (iTile / side) * gridSize;
            if (maze[iTile])
            {
                pendingCreation.Add(new Tile(gridIndex, TileType.stone_floor, newTilesContainer));
            }
            else
            {
                pendingCreation.Add(new Tile(gridIndex, wallType, newTilesContainer));
            }
        }
    }

    public Vector3 GetTargetClosestTo(Transform inTransform)
    {
        float closest = float.MaxValue;
        Vector3 result = inTransform.position;
        
        foreach(var entry in runes)
        {
            float distance = Vector3.Magnitude(entry.Value.transform.position - inTransform.position);
            if (entry.Value.activeSelf && distance <= closest)
            {
                closest = distance;
                result = entry.Value.transform.position;
            }
        }
        return result;
    }

    IEnumerator MoveTiles()
    {
        //yield return new WaitForEndOfFrame();
        Vector3 startPos = Vector3.up * 25f;
        newTilesContainer.position = startPos;
        float startTime = Time.time;
        while (Time.time < startTime + mMoveDuration)
        {
            newTilesContainer.position = Vector3.Lerp(startPos, Vector3.zero, (Time.time - startTime) * (Time.time - startTime) / (mMoveDuration * mMoveDuration));

            yield return new WaitForEndOfFrame();
        }

        newTilesContainer.position = tilesContainer.position;
        foreach (Tile tile in pendingCreation)
        {
            DestroyImmediate(tiles[tile.gridIndex].obj);
            tiles[tile.gridIndex] = tile;
            tile.transform.SetParent(tilesContainer);
            tilesType[tile.gridIndex] = tile.type;
        }
        pendingCreation.Clear();
        yield return 0;
    }
    /*
     * wikipedia prim algorithm
    Start with a grid full of walls.
    Pick a cell, mark it as part of the maze. Add the walls of the cell to the wall list.
    While there are walls in the list:
        Pick a random wall from the list.
            If only one of the two cells that the wall divides is visited, then:
                Make the wall a passage and mark the unvisited cell as part of the maze.
                Add the neighboring walls of the cell to the wall list.
        Remove the wall from the list.
*/
    static bool[] GenerateMaze(int mazeSize = 10)
    {
        bool[] grid = new bool[mazeSize * mazeSize];
        for (int y = 0; y < mazeSize; y++)
        {
            for (int x = 0; x < mazeSize; x++)
            {
                int iTile = x + y * mazeSize;
                grid[iTile] = false;
            }
        }

        List<int> walls = new List<int>();
        int toAdd = 1;
        grid[toAdd] = true;

        Neighbours n = new Neighbours(toAdd, mazeSize);
        AddToWalls(ref walls, grid, n);

        while (walls.Count > 0)
        {
            int wallIndex = Random.Range(0, walls.Count);
            if (grid[walls[wallIndex]])
            {
                walls.RemoveAt(wallIndex);
            }
            else
            {
                Neighbours wallNeighbours = new Neighbours(walls[wallIndex], mazeSize);
                if (wallNeighbours.up < mazeSize * mazeSize && wallNeighbours.down > 0 &&
                    wallNeighbours.right < mazeSize * mazeSize &&
                    wallNeighbours.left > 0)
                {
                    if (!grid[wallNeighbours.left] && !grid[wallNeighbours.right])
                    {
                        if (((grid[wallNeighbours.up] && !grid[wallNeighbours.down]) ||
                                (!grid[wallNeighbours.up] && grid[wallNeighbours.down])))
                        {
                            grid[walls[wallIndex]] = true;

                            AddToWalls(ref walls, grid, wallNeighbours);
                        }
                    }
                    else if (!grid[wallNeighbours.up] && !grid[wallNeighbours.down])
                    {
                        if (((grid[wallNeighbours.left] && !grid[wallNeighbours.right]) ||
                                (!grid[wallNeighbours.right] && grid[wallNeighbours.left])))
                        {
                            grid[walls[wallIndex]] = true;

                            AddToWalls(ref walls, grid, wallNeighbours);
                        }
                    }
                }
                walls.RemoveAt(wallIndex);
            }
        }
        for (int i = 0; i < mazeSize * mazeSize; i++)
        {
            if (i / mazeSize == 0 || i / mazeSize == mazeSize || i % mazeSize == 0 || i % mazeSize == mazeSize - 1)
            {
                grid[i] = false;
            }
        }

        //one entrance each side
        int entrance = Random.Range(1, mazeSize - 1);
        grid[entrance] = true;
        grid[entrance + mazeSize] = true;
        grid[entrance + mazeSize * 2] = true;
        grid[entrance + mazeSize * 3] = true;

        entrance = Random.Range(mazeSize * (mazeSize - 1) + 2, mazeSize * mazeSize - 1);
        grid[entrance] = true;
        grid[entrance - mazeSize] = true;
        grid[entrance - mazeSize * 2] = true;
        grid[entrance - mazeSize * 3] = true;

        entrance = mazeSize * Random.Range(1, mazeSize - 1);
        grid[entrance] = true;
        grid[entrance + 1] = true;
        grid[entrance + 2] = true;
        grid[entrance + 3] = true;

        entrance = -1 + mazeSize * Random.Range(1, mazeSize - 1);
        grid[entrance] = true;
        grid[entrance - 1] = true;
        grid[entrance - 2] = true;
        grid[entrance - 3] = true;

        return grid;
    }

    static void AddToWalls(ref List<int> walls, bool[] grid, Neighbours cellNeighbours)
    {
        if (cellNeighbours.up !=cellNeighbours.tile &&
            !grid[cellNeighbours.up])
        {
            walls.Add(cellNeighbours.up);
        }

        if (cellNeighbours.down !=cellNeighbours.tile &&
            !grid[cellNeighbours.down])
        {
            walls.Add(cellNeighbours.down);
        }
        if (cellNeighbours.left != cellNeighbours.tile &&
            !grid[cellNeighbours.left])
        {
            walls.Add(cellNeighbours.left);
        }
        if (cellNeighbours.right != cellNeighbours.tile &&
            !grid[cellNeighbours.right])
        {
            walls.Add(cellNeighbours.right);
        }
    }
}