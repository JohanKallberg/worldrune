﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    stone_floor,
    grass_floor,
    water,
    cliff,
    ice
};

public enum TileWorldFunction{
    nothing,
    pickable,
    minable,
    digable
}

//[CreateAssetMenu(fileName = "New TileType", menuName = "TileType", order = 51)]
public class TileTypeData //
{
    public int height = 0;
    public TileWorldFunction world;
    public TileTypeData(int tileHeight,TileWorldFunction tileWorld){//lets see how it turns out
        height = tileHeight;
        world = tileWorld;
    }

    public static Dictionary<TileType, TileTypeData> findData = new Dictionary<TileType, TileTypeData>{
        {TileType.stone_floor,new TileTypeData(0,TileWorldFunction.digable)},
        {TileType.grass_floor,new TileTypeData(0,TileWorldFunction.digable)},
        {TileType.water,new TileTypeData(-1,TileWorldFunction.nothing)},
        {TileType.cliff,new TileTypeData(1,TileWorldFunction.nothing)},
        {TileType.ice,new TileTypeData(1,TileWorldFunction.minable)}
    };
    private static Dictionary<TileType, GameObject> data;
    public static Dictionary<TileType, GameObject> types
    {
        get
        {
            if (data == null)
            {
                data = new Dictionary<TileType, GameObject>();
                data.Add(TileType.stone_floor, BaseTile(PrimitiveType.Quad, Color.grey));
                data.Add(TileType.grass_floor, BaseTile(PrimitiveType.Quad, Color.green));
                data.Add(TileType.water, BaseTile(PrimitiveType.Quad, Color.blue));
                data.Add(TileType.cliff, BaseTile(PrimitiveType.Cube, Color.black));
                data.Add(TileType.ice, BaseTile(PrimitiveType.Cube, Color.white));
            }
            return data;
        }
    }
    private static GameObject BaseTile(PrimitiveType primitive, Color col)
    {
        GameObject result = GameObject.CreatePrimitive(primitive);
        switch (primitive)
        {
            case PrimitiveType.Quad:
                result.transform.rotation = Quaternion.Euler(90, 0, 0);
                result.transform.localScale *= 2.5f;
                break;
            case PrimitiveType.Cube:
                result.transform.position += Vector3.up * 0.5f;
                result.transform.localScale *= 4f;
                break;
        }
        Object.Destroy(result.GetComponent<Collider>());
 
        Material mat = new Material(Shader.Find("Standard"));
        mat.color = col;
        result.GetComponent<Renderer>().material = mat;
        result.SetActive(false);
        return result;
    }
}
