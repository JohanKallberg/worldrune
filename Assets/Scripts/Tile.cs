using UnityEngine;
using System.Collections.Generic;

struct Neighbours
{
    public int[] array;
    
    public int tile{get {return array[0];}}
    public int up{get {return array[1];}}
    public int down{get {return array[2];}}
    public int left{get {return array[3];}}
    public int right{get {return array[4];}}
    public Neighbours(int tileIndex, int gridSize = TileGenerator.gridSize){
        array = new int[]{tileIndex, 
        Mathf.Clamp(tileIndex + gridSize,0,gridSize*gridSize),
        Mathf.Clamp(tileIndex - gridSize,0,gridSize*gridSize),
        Mathf.Clamp(tileIndex - 1,0,gridSize*gridSize),
        Mathf.Clamp(tileIndex + 1,0,gridSize*gridSize)};
    }
}
public class Tile
{
    public int gridIndex;
    public TileType type;
    public GameObject obj;
    public Transform transform;

    public Tile(int iTile, TileType type, Transform parent)
    {
        obj = Object.Instantiate<GameObject>(TileTypeData.types[type], parent);
        transform = obj.transform;
        transform.position = TileGenerator.CalcPosition(iTile);
        gridIndex = iTile;
        obj.SetActive(true);
        this.type = type;
    }
}