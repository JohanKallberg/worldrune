using UnityEngine;
using System.Collections.Generic;

public enum EntityType{ // HUM
  stone_source,
  grass_source,
  water_source,
  cliff_source,
  cliff_maze,
  cliff_maze_large,
  tool_shovel,
  rune_

  
};
public class Entity
{
    public TileType type;
    public GameObject obj;
    public Transform transform;

    public Entity(int iTile, TileType type, Transform parent)
    {
        obj = Object.Instantiate<GameObject>(TileTypeData.types[type], parent);
        transform = obj.transform;
        transform.position = TileGenerator.CalcPosition(iTile);
        obj.SetActive(true);
        this.type = type;
    }
}