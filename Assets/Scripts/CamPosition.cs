﻿using UnityEngine;

public class CamPosition : MonoBehaviour {

    public Transform character;

    private Vector3 initialOffset;
    void Start()
    {
        initialOffset = transform.position - character.position;

    }
    void Update () {
        transform.position = character.position + initialOffset;
	}
}
