﻿using UnityEngine;
using System.Collections.Generic;

public class Character : MonoBehaviour
{

    private Camera cam;
    
    public float baseMoveSpeed;
    public Transform target;
    public Transform head;
    private Plane ground;
    private bool drawNotMovingIndikator = false;

    private Transform m_transform;
    private TileGenerator gen;
    private int currentTile;
    private int targetTile;
    private Vector3 touchStartPos;
    private bool touchStart = false;
    void Start()
    {
        gen = FindObjectOfType<TileGenerator>();
        cam = Camera.main;
        m_transform = transform;
        ground = new Plane(Vector3.up, m_transform.position);
        currentTile = TileGenerator.FindTileIndex(m_transform.position);
        targetTile = currentTile;

    }

    void Update()
    {
        touchStart = Input.GetKeyDown(KeyCode.Mouse0);
        if (touchStart)
        {
            touchStartPos = Input.mousePosition;           
        }
        else if(Input.GetKey(KeyCode.Mouse0)&&!cam.pixelRect.Contains(Input.mousePosition )){
            var dir = Input.mousePosition-touchStartPos;
            Neighbours closeTiles = new Neighbours(currentTile);
            if( Mathf.Pow(dir.x,2)>Mathf.Pow(dir.y,2)){
                if(dir.x>0){
                    if(TileTypeData.findData[gen.tilesType[closeTiles.right]].height==0){
                        targetTile = closeTiles.right;
                    }
                }
                else if(dir.x<0){
                    targetTile = closeTiles.left;
                }
            }
            else{
                if(dir.y>0){
                    targetTile = closeTiles.up;
                }
                else if(dir.y<0){
                    targetTile = closeTiles.down;
                }
            }
        }
        if(    TileTypeData.findData[gen.tilesType[currentTile]].height!=
               TileTypeData.findData[gen.tilesType[targetTile ]].height){
            targetTile = currentTile;
        }
     
       Vector3 targetDir = (TileGenerator.CalcPosition(targetTile)-m_transform.position);
        if(targetTile != currentTile){
            Vector3 towardCurrentTile = (TileGenerator.CalcPosition(currentTile)-m_transform.position);
            if(Vector3.Dot(towardCurrentTile.normalized,targetDir.normalized)>0.01f){
                targetDir = towardCurrentTile;
            }
        }

        var possibleForward = Vector3.ProjectOnPlane(targetDir.normalized, Vector3.up);
        if(possibleForward != Vector3.zero) m_transform.forward = possibleForward;

        var moveSpeed = Mathf.Min(baseMoveSpeed* Time.deltaTime,targetDir.magnitude);
        m_transform.position += m_transform.forward * moveSpeed;
        currentTile = TileGenerator.FindTileIndex(m_transform.position);
        {
            Vector3 potentialLookat = gen.GetTargetClosestTo(head);
            if (potentialLookat == head.position)
            {
                return;
            }
            head.LookAt(potentialLookat);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        if (drawNotMovingIndikator)
        {
            Gizmos.color = new Color(1, 0, 1, 0.7F);
            Gizmos.DrawCube(m_transform.position + Vector3.up * 2f, Vector3.one);
        }
    }
}
