﻿using UnityEngine;

//[CreateAssetMenu(menuName="BaseRune")]
public class Rune : MonoBehaviour{
    public TileType type;
    public void Activate()
    {
        GameObject.FindObjectOfType<TileGenerator>().GenerateWorld(this); 
    }
    void Update(){
        transform.Rotate(0f,10f*Time.deltaTime,0f);
    }
}
