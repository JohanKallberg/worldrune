using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInteract : MonoBehaviour
{
    TileType toSpawn = TileType.water;
    public Button[] buttons;
    TileGenerator generator;
    int selected;
    Plane ground;// same as in character
    private Camera cam;
    void Start(){
        ground = new Plane(Vector3.up, transform.position);
        cam = Camera.main;
        generator = GameObject.FindObjectOfType<TileGenerator>();
        foreach(Button b in buttons){
            b.onClick.AddListener(() => ButtonClicked(b));
        }
    }

    void Update(){
        if (Input.GetKeyDown(KeyCode.Mouse0)&&cam.pixelRect.Contains(Input.mousePosition ))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            float rayDistance;
            if (ground.Raycast(ray, out rayDistance))
            {
                selected = TileGenerator.FindTileIndex(ray.GetPoint(rayDistance));
                if(generator.runes.ContainsKey(selected)){
                    generator.runes[selected].GetComponent<Rune>().Activate();
                }else{
                    generator.ReplaceTile(selected,toSpawn);
                }
            }
        }
    }

    void ButtonClicked(Button b){
        var uiTile = b.GetComponent<HotbarButton>();
        if(uiTile){
            toSpawn = uiTile.stack;
        }
    }    

    void OnDrawGizmos()
    {
        if(null == generator)
            return;
        
        Gizmos.color = new Color(1, 1, 0, 0.5F);
//        Gizmos.DrawCube(generator.tiles[selected].transform.position+Vector3.up, Vector3.one);
    }

}