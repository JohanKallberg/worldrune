using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleMob : MonoBehaviour
{
    public int gridIndex;
    //public GameObject obj;
    private float tileMoveDuration = 1f;

    private Transform m_transform;
    private TileGenerator gen;
    private int currentTile;
    private int targetTile;
    private Vector3 targetPosition;
    private Quaternion targetRotation;

    /* public SimpleMob(int iTile, TileType type, Transform parent)
     {
         obj = Object.Instantiate<GameObject>(TileTypeData.types[type], parent);
         transform = obj.transform;
         transform.position = TileGenerator.CalcPosition(iTile);
         gridIndex = iTile;
         obj.SetActive(true);
         this.type = type;
     }*/

    void Start()
    {
        gen = FindObjectOfType<TileGenerator>();
        m_transform = transform;
        targetRotation=m_transform.rotation;
        targetPosition = m_transform.position;
        currentTile = TileGenerator.FindTileIndex(m_transform.position);
        targetTile = currentTile;
        StartCoroutine(PickTile());
    }


    private IEnumerator PickTile()
    {
        while (enabled)
        {

            Neighbours closeTiles = new Neighbours(currentTile);
            targetTile = closeTiles.array[Random.Range(1, closeTiles.array.Length)];
            var currentType = gen.tilesType[currentTile];
            var currentPosition = TileGenerator.CalcPosition(currentTile);
            m_transform.position = new Vector3(currentPosition.x,m_transform.position.y,currentPosition.z); 
            var targetType = gen.tilesType[targetTile];
            targetPosition = TileGenerator.CalcPosition(targetTile);
            if (TileTypeData.findData[currentType].height !=
                TileTypeData.findData[targetType].height)
            {
                targetTile = currentTile;
            }
            else{
                targetRotation = Quaternion.LookRotation(targetPosition - m_transform.position, Vector3.up);
                
                m_transform.rotation = targetRotation;
            }
            yield return new WaitForSeconds(tileMoveDuration);
        }
        yield return 0;
    }
    void Update()
    {

        //  var randVec3 = new Vector3(Random.Range(-1f,1f),0f,Random.Range(-1f,1f))*TileGenerator.tileSize;
        //   var dir = m_transform.position + m_transform.forward*0.5f + randVec3;

        /*if( Mathf.Pow(dir.x,2)>Mathf.Pow(dir.y,2)){
             if(dir.x>0){
                 if(TileTypeData.findData[gen.tilesType[closeTiles.right]].height==0){
                     targetTile = closeTiles.right;
                 }
             }
             else if(dir.x<0){
                 targetTile = closeTiles.left;
             }
         }
         else{
             if(dir.y>0){
                 targetTile = closeTiles.up;
             }
             else if(dir.y<0){
                 targetTile = closeTiles.down;
             }
         }*/



       // m_transform.rotation = Quaternion.RotateTowards(m_transform.rotation,targetRotation,90*Time.deltaTime);

        if(Vector3.Dot(transform.forward,TileGenerator.CalcPosition(targetTile) - m_transform.position)>0){            
            var y = 1f-Vector2.Distance(new Vector2(targetPosition.x,targetPosition.z),
            new Vector2(m_transform.position.x,m_transform.position.z))/TileGenerator.tileSize;
            y = 0.5f - 2f*(y-0.5f)*(y-0.5f);
            var moveSpeed = TileGenerator.tileSize / tileMoveDuration * Time.deltaTime;
            m_transform.position = new Vector3(m_transform.position.x, y, m_transform.position.z) + m_transform.forward * moveSpeed;
        }
        
        currentTile = TileGenerator.FindTileIndex(m_transform.position);

    }
}